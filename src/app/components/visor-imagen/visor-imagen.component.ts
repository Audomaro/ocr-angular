import { Component, ElementRef, OnDestroy, OnInit, Output, ViewChild, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs';
import { OCRService } from 'src/app/services/ocr.service';
import * as Tesseract from 'tesseract.js';
import { recognize } from 'tesseract.js';

@Component({
  selector: 'app-visor-imagen',
  templateUrl: './visor-imagen.component.html',
  styleUrls: ['./visor-imagen.component.less']
})
export class VisorImagenComponent implements OnInit, OnDestroy {
  public subscripciones: Subscription[] = [];
  public url!: string;
  public cx!: CanvasRenderingContext2D;
  @ViewChild('imagenOriginal') imagenOriginal!: ElementRef;
  @ViewChild('imagenCanvas') imagenCanvas!: ElementRef;
  @Output() progresoAnalisis: EventEmitter<string> = new EventEmitter<string>();

  constructor(private ocrService: OCRService) { }

  ngOnInit(): void {
    const observable$ = this.ocrService.imagenAnalizar
      .subscribe((url: string) => {
        this.url = url;
      });

    this.subscripciones = [observable$];
  }

  ngOnDestroy(): void {
    this.subscripciones.forEach(subscripcion => subscripcion.unsubscribe());
  }

  public async analizarImagen(): Promise<void> {
    this.actualizarProgresoAnalisis('');
    this.ocrService.resultadoAnalisis.emit('');
    this.definirAreaDibujo();
    const resultadoAnalisis = await this.reconocerTexto();
    this.dibujarCuadros(resultadoAnalisis);
    this.ocrService.resultadoAnalisis.emit(resultadoAnalisis);
    this.actualizarProgresoAnalisis('');
  }

  private async reconocerTexto(): Promise<Tesseract.Page> {
    const imageElement = this.imagenOriginal.nativeElement;
    const { data } = await recognize(imageElement, 'spa', {
      logger: (tarea: any) => {
        this.actualizarProgresoAnalisis(`${tarea.status} - ${(tarea.progress * 100).toFixed(2)}%`);
      }
    });

    return data;
  }

  private actualizarProgresoAnalisis(progresoAnalisis: string): void {
    this.progresoAnalisis.emit(progresoAnalisis);
  }

  private definirAreaDibujo(): void {
    const imageElement = this.imagenOriginal.nativeElement;
    const canvasElement = this.imagenCanvas.nativeElement;
    canvasElement.width = imageElement.naturalWidth;
    canvasElement.height = imageElement.naturalHeight;
    this.cx = canvasElement.getContext('2d');
    this.cx.lineWidth = 2;
    this.cx.lineCap = 'square';
    this.cx.strokeStyle = 'red';
  }

  private dibujarCuadros(data: Tesseract.Page): void {
    data.words.forEach((palabra: any) => {
      const areaTexto = palabra.bbox;
      this.cx.strokeRect(areaTexto.x0, areaTexto.y0, areaTexto.x1 - areaTexto.x0, areaTexto.y1 - areaTexto.y0);
      this.cx.beginPath();
      this.cx.stroke();
    });
  }
}
