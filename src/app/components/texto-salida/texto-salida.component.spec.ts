import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TextoSalidaComponent } from './texto-salida.component';

describe('TextoSalidaComponent', () => {
  let component: TextoSalidaComponent;
  let fixture: ComponentFixture<TextoSalidaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TextoSalidaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TextoSalidaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
