import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { OCRService } from 'src/app/services/ocr.service';

@Component({
  selector: 'app-texto-salida',
  templateUrl: './texto-salida.component.html',
  styleUrls: ['./texto-salida.component.less']
})
export class TextoSalidaComponent implements OnInit, OnDestroy {
  private subscripciones: Subscription[] = [];
  public textoSalida = '';

  constructor(private ocrService: OCRService) {
  }

  ngOnInit(): void {
    this.subscribirseObservables();
  }

  ngOnDestroy(): void {
    this.subscripciones.forEach(subscripcion => subscripcion.unsubscribe());
  }

  private subscribirseObservables(): void {
    const observables$ = this.ocrService.resultadoAnalisis
      .subscribe(({ text }: any) => {
        this.textoSalida = text;
      });

    this.subscripciones = [observables$];
  }
}
