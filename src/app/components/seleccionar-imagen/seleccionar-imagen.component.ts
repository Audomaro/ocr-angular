import { Component, OnInit } from '@angular/core';
import { ArchivosService } from 'src/app/services/archivos.service';
import { OCRService } from 'src/app/services/ocr.service';

@Component({
  selector: 'app-seleccionar-imagen',
  templateUrl: './seleccionar-imagen.component.html',
  styleUrls: ['./seleccionar-imagen.component.less']
})
export class SeleccionarImagenComponent implements OnInit {

  public opcionesCarga = [{
    id: 1,
    descripcion: 'Imagen precargada'
  }, {
    id: 2,
    descripcion: 'Cargar imagen'
  }];

  public archivos: any[] = [];

  public isDropZoneActive = false;

  constructor(private ocrService: OCRService, private archivosService: ArchivosService) {
    this.onDropZoneEnter = this.onDropZoneEnter.bind(this);
    this.onDropZoneLeave = this.onDropZoneLeave.bind(this);
  }

  ngOnInit(): void {
    this.archivos = this.archivosService.listar();
  }

  public iconoPersonalizado(archivo: any): string {
    const extencion = archivo.getFileExtension();
    let icono = '';

    switch (extencion) {
      case '.jpg':
        icono = './assets/icons/jpg_2.png';
        break;

      case '.png':
        icono = './assets/icons/png_2.png';
        break;

      case '.svg':
        icono = './assets/icons/svg_2.png';
        break;
    }

    return icono;
  }

  public cargarImagen(e: any): void {
    const origen = `./assets/images/${e.file.name}`;
    this.ocrService.imagenAnalizar.emit(origen);
  }

  public onDropZoneEnter(e: any): void {
    if (e.dropZoneElement.id === 'dropzone-external') {
      this.isDropZoneActive = true;
    }

  }

  public onDropZoneLeave(e: any): void {
    if (e.dropZoneElement.id === 'dropzone-external') {
      this.isDropZoneActive = false;
    }
  }
}
