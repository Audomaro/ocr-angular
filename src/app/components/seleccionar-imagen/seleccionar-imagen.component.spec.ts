import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SeleccionarImagenComponent } from './seleccionar-imagen.component';

describe('SeleccionarImagenComponent', () => {
  let component: SeleccionarImagenComponent;
  let fixture: ComponentFixture<SeleccionarImagenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SeleccionarImagenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SeleccionarImagenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
