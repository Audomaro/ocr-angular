import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-tarjeta',
  templateUrl: './tarjeta.component.html',
  styleUrls: ['./tarjeta.component.less']
})
export class TarjetaComponent implements OnInit {
  @Input() titulo = '';
  @Input() verPie = false;
  @Input() pie = '';
  @Input() alto: string = 'auto';
  @Input() ancho: string = 'auto';

  constructor() { }

  ngOnInit(): void {
  }

}
