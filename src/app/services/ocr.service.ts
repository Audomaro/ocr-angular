import { EventEmitter, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class OCRService {

  public imagenAnalizar: EventEmitter<any> = new EventEmitter<any>();
  public resultadoAnalisis: EventEmitter<any> = new EventEmitter<any>();

  constructor() { }
}
