import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ArchivosService {

  constructor() { }

  public listar(): any[] {
    const archivos = [
      ...this.generarListaArchivos('jpg', 6),
      ...this.generarListaArchivos('png', 7),
      ...this.generarListaArchivos('svg', 3),
    ];

    return archivos;
  }

  private generarListaArchivos(tipo: string, cantidad: number): any[] {
    const archivos = [];

    for (let i = 1; i <= cantidad; i++) {
      archivos.push({
        name: `${i}.${tipo}`
      });
    }

    return archivos;
  }
}
