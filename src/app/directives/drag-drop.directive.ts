import { Directive, ElementRef, EventEmitter, HostBinding, HostListener, Output, Renderer2 } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

import { ArchivoInterface } from '../interfaces/archivo.interface';
import { OCRService } from '../services/ocr.service';

@Directive({
  selector: '[appDragDrop]'
})
export class DragDropDirective {

  @HostBinding('style.background')
  private background = '#eee';

  constructor(private sanitizer: DomSanitizer, private el: ElementRef, private renderer: Renderer2, private ocrService: OCRService) {
    this.renderer.setStyle(this.el.nativeElement, 'border-width', '5px');
    this.renderer.setStyle(this.el.nativeElement, 'border-style', 'dashed');
    this.renderer.setStyle(this.el.nativeElement, 'border-color', '#ccc');
  }

  @HostListener('dragover', ['$event'])
  // tslint:disable-next-line: typedef
  public onDragOver(evt: DragEvent) {
    evt.preventDefault();
    evt.stopPropagation();
    this.renderer.setStyle(this.el.nativeElement, 'border-style', 'solid');
    this.renderer.setStyle(this.el.nativeElement, 'border-color', '#337ab7');
  }

  @HostListener('dragleave', ['$event'])
  // tslint:disable-next-line: typedef
  public onDragLeave(evt: DragEvent) {
    evt.preventDefault();
    evt.stopPropagation();
    this.renderer.setStyle(this.el.nativeElement, 'border-style', 'dashed');
    this.renderer.setStyle(this.el.nativeElement, 'border-color', '#ddd');
  }

  @HostListener('drop', ['$event'])
  // tslint:disable-next-line: typedef
  public onDrop(evt: DragEvent) {
    evt?.preventDefault();
    evt?.stopPropagation();
    this.renderer.setStyle(this.el.nativeElement, 'border-style', 'dashed');
    this.renderer.setStyle(this.el.nativeElement, 'border-color', '#ddd');

    if (evt && evt.dataTransfer && evt.dataTransfer.files.length === 1) {
      const archivo = evt?.dataTransfer?.files[0];
      const url = this.sanitizer.bypassSecurityTrustUrl(window.URL.createObjectURL(archivo));

      if (archivo.type.match(/image+/)) {
        this.ocrService.imagenAnalizar.emit(url);
      }
    }
  }
}
