import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { DxFileManagerModule, DxTabPanelModule, DxTemplateModule, DxTextAreaModule } from 'devextreme-angular';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { SeleccionarImagenComponent } from './components/seleccionar-imagen/seleccionar-imagen.component';
import { SpinnerComponent } from './components/spinner/spinner.component';
import { TarjetaComponent } from './components/tarjeta/tarjeta.component';
import { TextoSalidaComponent } from './components/texto-salida/texto-salida.component';
import { VisorImagenComponent } from './components/visor-imagen/visor-imagen.component';
import { DragDropDirective } from './directives/drag-drop.directive';

@NgModule({
  declarations: [
    AppComponent,
    DragDropDirective,
    TextoSalidaComponent,
    TarjetaComponent,
    HeaderComponent,
    VisorImagenComponent,
    SpinnerComponent,
    SeleccionarImagenComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    DxFileManagerModule,
    DxTabPanelModule,
    DxTemplateModule,
    DxTextAreaModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
