import { SafeUrl } from '@angular/platform-browser';

export interface ArchivoInterface {
  archivo: File;
  url: SafeUrl
}
